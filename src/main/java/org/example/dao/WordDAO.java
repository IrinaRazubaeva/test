package org.example.dao;

import org.example.models.Word;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;


@Repository
public class WordDAO {
    private static final String URL = "jdbc:postgresql://127.0.0.1:5432/unique_words";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "kompot";

    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static Connection connection;

    /**
     * Метод, в котором сохраняются данные из базы данных в переменную типа "ArrayList"
     * @return listWord - переменная, которая хранит в себе данные из бд
     */
    public ArrayList<Word> getListWord() {
        ArrayList<Word> listWord = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM test");

            while(resultSet.next()) {
                Word word = new Word();

                word.setId(resultSet.getInt("id"));
                word.setName(resultSet.getString("word"));
                word.setCnt(resultSet.getInt("cnt"));

                listWord.add(word);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return listWord;
    }

    /**
     * Метод, в котором данные из переменной типа "ArrayList" записываются в базу данных
     * @param arrayList - переменная, в которой хранятся данные для записи
     */
    public void save(ArrayList<String> arrayList){
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM test");
            statement.executeUpdate("TRUNCATE TABLE test RESTART IDENTITY;");
            String name;
            int cnt=0;
            while (arrayList.size() > 0) {
                name = arrayList.get(0);
                int index = 0;
                cnt = 0;
                while (arrayList.contains(name)) {
                    index = arrayList.indexOf(name);
                    arrayList.remove(index);
                    cnt++;
                }
                String SQL = "INSERT INTO test (word, cnt) VALUES ('" + name + "', " + cnt + ")";
                statement.executeUpdate(SQL);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
