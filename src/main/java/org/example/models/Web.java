package org.example.models;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Web {


    private String nameWeb;
    private String titleUrl;
    private String bodyUrl;
    private ArrayList <String> listBody;

    public Web(){
    }

    public Web(String nameWeb){
        this.nameWeb = nameWeb;
    }

    public String getNameWeb() {
        return nameWeb;
    }
    public void setNameWeb(String nameWeb) {
        this.nameWeb = nameWeb;
    }

    public ArrayList<String> getListBody() {
        return listBody;
    }

    /**
     * Метод, в котором происходит загрузка заголовка и текста страницы
     */
    public void getHtml(){
        Document document = null;
        try {
            document = Jsoup.connect(nameWeb).get();
            titleUrl = document.title();
            bodyUrl = document.text();
            //urlError = "Загрузка страницы - успешено";
        } catch (Exception e) {
            //urlError = e.getMessage() + " Неудачное соединение или адрес введен неверно";
            titleUrl = "error";
        }

    }

    /**
     * Метод, в котором создается переменная типа "ArrayList". В ней хранятся все слова текста страницы
     */
    public void calculate(){
        if (titleUrl.equals("error")){
            return;
        }
        String[] temp = bodyUrl.split("[\\s \",.?!;:$&\'[ ](){}/<>\n\r\t]");
        listBody = new ArrayList<String>(Arrays.asList(temp));
        temp = new String[]{"", "*", "**", "+", "-", "="};
        Collections.sort(listBody);
        for (String del : temp) {
            removeTrash(del, listBody);
        }

    }

    /**
     * Метод, в котором происходит удаление указанного слова (или слов, если оно повторяется) из указанного списка
     * @param str - переменная, хранящее слово, которое нужно удалить
     * @param list - список, из которого нужно удалить слово
     */
    public void removeTrash(String str, ArrayList<String> list) {
        int index = 0;
        while (list.contains(str)) {
            index = list.indexOf(str);
            list.remove(index);
        }
    }


}
