package org.example.controllers;

import org.example.dao.WordDAO;
import org.example.models.Web;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
public class ParsingController {
    private final WordDAO wordDAO;

    @Autowired
    public ParsingController(WordDAO wordDAO){
        this.wordDAO = wordDAO;
    }

    @GetMapping()
    public String newString(@ModelAttribute("web") Web web){
        web.setNameWeb("https://www.simbirsoft.com/");
        return "/index";
    }
    @PostMapping("/index")
    public String webCreate ( Web web,Model model){
        web.getHtml();
        web.calculate();
        wordDAO.save(web.getListBody());
        ArrayList list = wordDAO.getListWord();
        model.addAttribute("list",list);
        return "/new";
    }
}
